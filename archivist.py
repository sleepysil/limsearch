import argparse
import os
import sys

from juna.archivist import index_pages
from juna.dataspace import postgres_interface

parser = argparse.ArgumentParser(
    prog='archivist.py',
    description='Indexes content from a Wikidot wiki (by default, Liminal Archives).',
    epilog='Created by Sleepy Sil')

parser.add_argument('-u', '--url', help='The URL of the wiki to index.', default="liminal-archives.wikidot.com",
                    required=False)
parser.add_argument('-d', '--database', help='The name of the database.', default="dataspace",
                    required=False)
parser.add_argument('-U', '--user', help='The user to log into the database as.', default="juna",
                    required=False)
parser.add_argument('-H', '--host', help='The database server host.', default="localhost",
                    required=False)
parser.add_argument('-p', '--port', help='The database server port.', default="5432",
                    required=False)
parser.add_argument('-i', '--init', help='Initialize the index instead of updating.', default=False,
                    action='store_true', required=False)
parser.add_argument('-v', '--verbose', action='count', default=0, help="Increase debug logging verbosity.")

args = parser.parse_args()

try:
    port_num = int(args.port)
except ValueError:
    sys.stderr.write(f"error: port '{args.port}' is not a number")
    sys.exit(1)

password = None
if "JUNA_DB_PASS" in os.environ:
    password = os.environ["JUNA_DB_PASS"]

psql = postgres_interface.PostgresInterface(args.database, args.user, password, args.host, port_num)

if args.init:
    index_pages.init_index(args.url, psql, verbosity=args.verbose)
else:
    index_pages.update_index(args.url, psql, verbosity=args.verbose)
