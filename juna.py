import argparse
import os
import sys

from juna.juna import search


parser = argparse.ArgumentParser(
    prog='juna.py',
    description='Searches an index created by archivist.',
    epilog='Created by Sleepy Sil')

parser.add_argument('query', nargs='?', help='The search query. Omit to run in interactive mode.')
parser.add_argument('-d', '--database', help='The name of the database.', default="dataspace",
                    required=False)
parser.add_argument('-U', '--user', help='The user to log into the database as.', default="juna",
                    required=False)
parser.add_argument('-H', '--host', help='The database server host.', default="localhost",
                    required=False)
parser.add_argument('-p', '--port', help='The database server port.', default="5432",
                    required=False)


args = parser.parse_args()

try:
    port_num = int(args.port)
except ValueError:
    sys.stderr.write(f"error: port '{args.port}' is not a number")
    sys.exit(1)

password = None
if "JUNA_DB_PASS" in os.environ:
    password = os.environ["JUNA_DB_PASS"]

search_handler = search.SearchHandler(args.database, args.user, password, args.host, port_num)

if args.query is None:
    try:
        while True:
            input_query = input("query: ")
            print(search.discord_format_query(search_handler, input_query))
    except KeyboardInterrupt:
        print()
        sys.exit(0)
else:
    search.discord_format_query(search_handler, args.query)
