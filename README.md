Search for Liminal Archives (though other Wikidot wikis should work as well), still WIP

Depends on:
* beautifulsoup4 https://pypi.org/project/beautifulsoup4/ (HTML parsing)
* psycopg2 https://pypi.org/project/psycopg2/ (PostgreSQL interface)
* postgresql https://www.postgresql.org/ (Database software)
* requests https://pypi.org/project/requests/ (HTTP requests)
* discord.py https://pypi.org/project/discord.py/ (Discord bot API)

To run the bot, define `DISCORD_TOKEN` and `JUNA_DB_PASS` in a `.env` (see the `.env.example` for a model), and run `juna.sh`

`juna_bot.py` is the bot's source code, but `juna.sh` sets the token in the environment for you before starting it.

To create/update the index, run `archivist.py`. You should run this daily, as the incremental update is designed for daily updates.

`juna.py` is a local CLI search for debugging and development.