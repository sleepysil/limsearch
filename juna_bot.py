import argparse
import datetime
import os
import json
import sys

import discord
from discord import app_commands

from juna.juna import search
from juna.juna.search import QuerySyntaxError

parser = argparse.ArgumentParser(
    prog='juna_bot.py',
    description='Searches an index created by archivist.',
    epilog='Created by Sleepy Sil')

parser.add_argument('-d', '--database', help='The name of the database.', default="dataspace",
                    required=False)
parser.add_argument('-U', '--user', help='The user to log into the database as.', default="juna",
                    required=False)
parser.add_argument('-H', '--host', help='The database server host.', default="localhost",
                    required=False)
parser.add_argument('-p', '--port', help='The database server port.', default="5432",
                    required=False)

args = parser.parse_args()

try:
    port_num = int(args.port)
except ValueError:
    sys.stderr.write(f"error: port '{args.port}' is not a number")
    sys.exit(1)

password = None
if "JUNA_DB_PASS" in os.environ:
    password = os.environ["JUNA_DB_PASS"]

search_handler = search.SearchHandler(args.database, args.user, password, args.host, port_num)

systemcon_points = {":yellowteam:": 0, ":greenteam:": 0, ":blueteam:": 0, ":redteam:": 0}

if not os.path.isfile("systemcon_points.txt"):
    with open("systemcon_points.txt", "w") as f:
        json.dump(systemcon_points, f)
else:
    with open("systemcon_points.txt") as f:
        systemcon_points = json.load(f)


def get_emoji(name, bot):
    emoji = discord.utils.get(bot.emojis, name=name.replace(':', ''))
    if emoji is None:
        emoji = name
    return emoji


def get_emoji_name(emoji):
    if emoji.startswith("<"):
        return ":" + emoji.split(":")[1] + ":"
    else:
        return emoji


class JunaClient(discord.Client):
    async def on_ready(self):
        """Runs when the bot logs into Discord"""
        # update slash commands
        await tree.sync()
        print('Logged on as', self.user)


intents = discord.Intents.default()
client = JunaClient(intents=intents)
tree = app_commands.CommandTree(client)


@tree.command(
    name="search",
    description="search the Liminal Archives",
)
async def search_cmd(interaction, query: str, max_results: int = 10):
    try:
        results = search.discord_format_query(search_handler, query, max_results=max_results)
    except QuerySyntaxError as e:
        await interaction.response.send_message(str(e), ephemeral=True)
        return
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Search results for `{query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="title",
    description="search the Liminal Archives by title",
)
async def title_cmd(interaction, query: str, max_results: int = 10):
    try:
        results = search.discord_format_query(search_handler, title_query=query, max_results=max_results)
    except QuerySyntaxError as e:
        await interaction.response.send_message(str(e), ephemeral=True)
        return
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Title search results for `{query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="article",
    description="search Liminal Archives article pages",
)
async def article(interaction, query: str, max_results: int = 10):
    try:
        results = search.discord_format_query(search_handler, text_query=query, tags_query=['article'],
                                              max_results=max_results)
    except QuerySyntaxError as e:
        await interaction.response.send_message(str(e), ephemeral=True)
        return
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Search results for `{query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="show",
    description="provide quick info on a Liminal Archives page",
)
async def article(interaction, pagename: str):
    page_id = search_handler.psql.get_page_id_for_title(pagename)

    if page_id is None:
        await interaction.response.send_message(f"Error: page `{pagename}` not found.", ephemeral=True)

    else:
        data = search_handler.psql.get_page_rec(page_id)
        upvotes = sum([rec[0] for rec in data['upvoters']])
        downvotes = sum([rec[0] for rec in data['downvoters']])
        rating = upvotes+downvotes
        rating_str = "+"+str(rating) if rating >= 0 else str(rating)
        output = f"## *{data['title']}*, by {data['poster']}, {rating_str} [+{upvotes}, -{abs(downvotes)}]\n"
        output += f"* **URL:** {data['url']}\n"
        output += f"* **Canon:** {'Canon' if data['canon'] else 'Non-canon'}\n"
        output += f"* **Tags:** {' '.join([rec[0] for rec in data['tags']])}\n"
        created_time_fmt = data['created_at'].astimezone(
            datetime.timezone(datetime.timedelta())).strftime('%Y-%m-%d %H:%M')
        output += f"* **Created:** {created_time_fmt}\n"
        updated_time_fmt = data['updated_at'].astimezone(
            datetime.timezone(datetime.timedelta())).strftime('%Y-%m-%d %H:%M')
        output += f"* **Last edited:** {updated_time_fmt}\n\n"
        output += f"* **Upvoters:** {', '.join([rec[2] for rec in data['upvoters']])}\n"
        output += f"* **Downvoters:** {', '.join([rec[2] for rec in data['downvoters']])}\n"
        last_updated = search_handler.psql.get_last_updated()
        embed = discord.Embed(title=f"Info for `{pagename}`", description=output).set_footer(
            text=f"Last updated: {last_updated}")
        await interaction.response.send_message(embed=embed)


@tree.command(
    name="quicksearch",
    description="search Liminal Archives article pages, showing the top hit only.",
)
async def quicksearch(interaction, query: str):
    try:
        results = search.discord_format_query(search_handler, text_query=query, tags_query=['article'], max_results=1)
    except QuerySyntaxError as e:
        await interaction.response.send_message(str(e), ephemeral=True)
        return
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Search result for `{query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="quicktitle",
    description="search Liminal Archives article pages by title, showing the top hit only.",
)
async def quicktitle(interaction, query: str):
    try:
        results = search.discord_format_query(search_handler, title_query=query, tags_query=['article'], max_results=1)
    except QuerySyntaxError as e:
        await interaction.response.send_message(str(e), ephemeral=True)
        return
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Title search result for `{query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="author",
    description="search for pages by author",
)
async def author(interaction, query: str, max_results: int = 10):
    orig_query = query
    # handle spaces in usernames
    if '"' not in query and " " in query:
        query = '"' + query + '"'
    results = search.discord_format_query(search_handler, author_query=query, max_results=max_results)
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Pages by `{orig_query}`", description=results).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="topauthors",
    description="list top authors by page count",
)
async def topauthors(interaction, max_results: int = 10):
    result = search_handler.psql.exec_sql(
        "select name, count(*) as num_pages from users inner join (select author_id, title "
        "from pages inner join pages_tags on pages_tags.page_id=pages.id inner join tags on "
        "tags.id=pages_tags.tag_id where tags.name = 'article') as articles on "
        "articles.author_id=users.id group by name order by num_pages desc LIMIT %s;",
        max_results)

    formatted = ""
    for author_rec in result:
        formatted += "* "+author_rec[0]+": "+str(author_rec[1])+"\n"
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"Top {max_results} authors by page count", description=formatted).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="source",
    description="links Juna's source code",
)
async def source(interaction):
    embed = discord.Embed(title=f"Source code",
                          description="The source for the search system is available at \
                          https://gitlab.com/sleepysil/limsearch")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="award",
    description="award Systemcon 2024 points to a team",
)
async def award(interaction, team: str, points: int, member: str, reason: str, link: str = None):
    team_name = get_emoji_name(team)
    await interaction.response.send_message(f"Error: the contest has ended", ephemeral=True)
    return

    if team_name not in systemcon_points:
        interaction.command_failed = True
        await interaction.response.send_message(f"Error: unrecognized team {team}", ephemeral=True)
        return
    msg = f"## {team} + {points} points:\n*{reason} from {member}*"
    if link is not None:
        msg += f"\n{link}"
    systemcon_points[get_emoji_name(team)] += points
    await interaction.response.send_message(msg)


@tree.command(
    name="steal",
    description="steal Systemcon 2024 points from a team for another",
)
async def steal(interaction, team_to: str, team_from: str, points: int,
                member: str, reason: str, link: str = None):
    await interaction.response.send_message(f"Error: the contest has ended", ephemeral=True)
    return

    for team in [team_from, team_to]:
        team_name = get_emoji_name(team)
        if team_name not in systemcon_points:
            interaction.command_failed = True
            await interaction.response.send_message(f"Error: unrecognized team {team}", ephemeral=True)
            return
    msg = f"## {team_from} - {points} points, {team_to} + {points} points:\n*{reason} from {member}*"
    if link is not None:
        msg += f"\n{link}"
    systemcon_points[get_emoji_name(team_from)] -= points
    systemcon_points[get_emoji_name(team_to)] += points
    await interaction.response.send_message(msg)


@tree.command(
    name="totals",
    description="print Systemcon 2024 point totals",
)
async def totals(interaction):
    msg = "## Totals:\n"
    ranking = sorted(systemcon_points, key=systemcon_points.get, reverse=True)
    i = 1
    for team in ranking:
        msg += f"* #{i}: {get_emoji(team, interaction.client)}: {systemcon_points[team]} points\n"
        i += 1
    await interaction.response.send_message(msg)


@tree.command(
    name="dump",
    description="get latest Juna data dump",
)
async def dump(interaction):
    dump_path = os.path.join(os.environ["DUMP_PATH"], 'latest_dump.txt')
    with open(dump_path) as latest:
        latest_date = latest.read().strip()

    dump_path = os.path.join(os.environ["DUMP_PATH"], latest_date+"_dump.sql.gz")

    embed = discord.Embed(title=f"{latest_date} Juna database dump",
                          description="To properly use this database dump, create a "
                                      "PostgreSQL database with a user named `juna` "
                                      f", unzip the dump, and run "
                                      f"`psql -d <database name> -U juna < {latest_date+'_dump.sql'}`.")
    dump_file = discord.File(dump_path, latest_date+"_dump.sql.gz")
    await interaction.response.send_message(embed=embed, file=dump_file)


@tree.command(
    name="recentvotes",
    description="print recent votes",
)
async def recentvotes(interaction, max_results: int = 10):
    result = search_handler.psql.exec_sql(
        "select users.name, pages.title, ratings.value, ratings.seen_at, pages.url "
        "from ratings inner join pages on pages.id=ratings.page_id inner join users "
        "on users.id=ratings.user_id order by ratings.seen_at desc limit %s;",
        max_results)

    formatted = ""
    for rec in result:
        if rec[2] > 0:
            formatted += f"* {get_emoji(':upvote:', interaction.client)} "
        elif rec[2] < 0:
            formatted += f"* {get_emoji(':downvote:', interaction.client)} "
        else:
            formatted += "* ❓ "
        formatted += f"**{rec[0]}** on: [*{rec[1]}*]({rec[4]}) " \
                     f"({rec[3].astimezone(datetime.timezone(datetime.timedelta())).strftime('%Y-%m-%d')})\n"
    last_updated = search_handler.psql.get_last_updated()
    embed = discord.Embed(title=f"{max_results} most recent votes", description=formatted).set_footer(
        text=f"Last updated: {last_updated}")
    await interaction.response.send_message(embed=embed)


@tree.command(
    name="say",
    description="make Juna say something",
)
async def say(interaction, msg: str):
    await interaction.response.send_message(msg)


client.run(os.environ['DISCORD_TOKEN'])

with open("systemcon_points.txt", "w") as f:
    json.dump(systemcon_points, f)
