import datetime
import sys

from juna.archivist import fetch_data, wikidot_interface


def update_pages(interface, pages, new_update_time):
    """Updates the given pages in the database"""
    for page_url in pages:
        page = pages[page_url]
        # add or update the page itself and author data
        interface.create_or_update_page(page)
        ratings = process_ratings(page['who_rated'])
        # add or update rating data
        interface.create_or_update_rating(ratings, page['pageId'], new_update_time)


def process_ratings(who_rated):
    """Convert rating data parsed from Wikidot into the right format to add to the database"""
    ratings = []
    for rating in who_rated:
        rating_str = who_rated[rating]['rating']  # "+" for upvote, "-" for downvote
        rating_num = 0
        if rating_str == "+":
            rating_num = 1
        elif rating_str == "-":
            rating_num = -1
        # assemble rating record dict
        ratings.append({'user_name': who_rated[rating]['name'], 'user_id': rating, 'value': rating_num})
    return ratings


def init_index(url, psql, verbosity=0):
    """Initialize the index from scratch"""
    interface = wikidot_interface.WikidotInterface(url, verbosity)
    new_update_time = datetime.datetime.now()

    # get pages from wiki
    pages = fetch_data.get_all_pages(interface)

    if interface.verbosity > 0:
        sys.stderr.write(f"Updating database\n")

    # add pages to db
    update_pages(psql, pages, new_update_time)

    # update last updated time
    psql.set_last_update_time(new_update_time)
    psql.commit()


def update_index(url, psql, verbosity=0):
    """Update the index with the last day's changes"""
    interface = wikidot_interface.WikidotInterface(url, verbosity)
    new_update_time = datetime.datetime.now()

    # last_update_rec = psql.exec_sql("SELECT last_updated FROM metadata;")
    # last_update = None
    # if len(last_update_rec) > 0:
    # last_update = last_update_rec[0][0]

    specifier = "last day"

    # if last_update is not None:
    # specifier = f">={last_update.astimezone(datetime.timezone(datetime.timedelta()))}"

    # get pages updated in the last day (daily index update)
    # TODO change this to pull everything updated since the last index update
    pages = fetch_data.get_all_pages(interface, updated_at=specifier)

    # get some quick data (url, votes) for every page
    metadata = fetch_data.get_quick_data(interface)

    # get urls for every page in the database
    # unlike metadata, these are not the usual page dictionaries, these are tuples of (url, pageId)
    db_pages = psql.exec_sql("SELECT url, id FROM pages;")

    # every page with a url in db_pages but not metadata (either deleted or moved)
    deleted_or_moved_pages = [page[1] for page in db_pages if page[0] not in metadata]

    # check if the "deleted" pages still exist by id
    existences = fetch_data.check_page_existences(interface, [page for page in deleted_or_moved_pages])

    if interface.verbosity > 0:
        sys.stderr.write(f"Processing page deletions\n")

    # check potential page deletions
    for page in deleted_or_moved_pages:
        if not existences[page] and existences[page] is not None:
            # if the page doesn't exist by ID, mark it deleted
            psql.delete_page(page)

    if interface.verbosity > 0:
        sys.stderr.write(f"Processing rating updates\n")

    # check rating updates for all pages
    for page_url in metadata:
        page_id = psql.get_page_id(page_url)
        if page_id is None:
            continue  # page not in DB properly
        page = metadata[page_url]
        if int(page['rating']) != int(psql.get_page_rating(page_id)):
            # if the rating total has changed, update the ratings
            ratings = process_ratings(fetch_data.get_rating_data(page_id, interface))
            psql.create_or_update_rating(ratings, page_id, new_update_time)

    if interface.verbosity > 0:
        sys.stderr.write(f"Updating database\n")

    # record new/edited pages in DB
    update_pages(psql, pages, new_update_time)

    # update last updated time
    psql.set_last_update_time(new_update_time)
    psql.commit()
