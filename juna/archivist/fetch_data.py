import time
import datetime
import sys

from juna.archivist import wikidot_interface


def get_all_pages(interface, **kwargs):
    """Gets every page on the wiki and its metadata, including page ID"""
    # Wikidot syntax for output format, see https://www.wikidot.com/doc-modules:listpages-module
    # The blank line before %%content%% causes a paragraph break (i.e. a new <p> tag), making parsing easier
    # |$$| is an arbitrary delimiter I use to make splitting easier
    module_body = "%%title%%|$$|%%tags%%|$$|%%rating%%|$$|%%rating_votes%%|$$|%%created_by%%|$$|%%created_by_id%%\
                  |$$|%%revisions%% %%created_at%% %%updated_at%% %%link%% "

    if interface.verbosity > 0:
        sys.stderr.write(f"Fetching metadata with ListPages")
        sys.stderr.flush()
    if interface.verbosity >= 2:
        sys.stderr.write("\n")

    # get list of parsed HTML responses for each page of the listpages output
    soups = interface.paginate_module("list/ListPagesModule", perPage=200, module_body=module_body, **kwargs)

    if interface.verbosity > 0:
        sys.stderr.write(f"Processing page metadata\n")

    data = {}
    # loop over the paginated result
    for soup in soups:
        # get each page in the list (non-recursive to avoid bugs with nested listpages on hubs)
        entries = soup.div.find_all(class_="list-pages-item", recursive=False)
        for entry in entries:
            # get the first <p> element (containing the metadata info)
            metadata_raw = entry.p.contents
            # remove the last delimiter and split
            metadata_arr = metadata_raw[0].strip().split("|$$|")
            rating = int(metadata_arr[2])
            total_votes = int(metadata_arr[3])
            # url is the only <a> element in there
            created_at_timestamp = None
            for created_at_class in metadata_raw[1]['class']:
                if created_at_class.startswith("time_"):
                    created_at_timestamp = int(created_at_class.replace("time_", ""))
            updated_at_timestamp = None
            for updated_at_class in metadata_raw[3]['class']:
                if updated_at_class.startswith("time_"):
                    updated_at_timestamp = int(updated_at_class.replace("time_", ""))
            # convert author id to int, or None if missing
            try:
                author_id = int(metadata_arr[5])
            except ValueError:
                author_id = None
            revisions = int(metadata_arr[6])
            metadata = {"title": metadata_arr[0], "tags": metadata_arr[1].split(" "), "rating": rating,
                        "votes": total_votes, "author_name": metadata_arr[4], "author_id": author_id,
                        "url": entry.p.a.string, "created_at": datetime.datetime.fromtimestamp(created_at_timestamp),
                        "updated_at": datetime.datetime.fromtimestamp(updated_at_timestamp), "revisions": revisions}
            # the page is non-canon if it has the "non-canon" tag
            metadata["canon"] = "non-canon" not in metadata["tags"]
            data[metadata["url"]] = metadata

    # request rate limit
    time.sleep(1.0 / interface.request_rate)

    if interface.verbosity > 0:
        sys.stderr.write(f"Fetching full data for {len(data)} pages")
        sys.stderr.flush()
    if interface.verbosity >= 2:
        sys.stderr.write("\n")

    # fetch metadata for each page (1 request per page)
    full_data = get_pages_info(interface, data.keys())

    # counts used for verbose logging
    i = 1
    total = len(full_data)

    if interface.verbosity > 0:
        sys.stderr.write(f"Fetching ratings and source for {total} pages")
        sys.stderr.flush()

    for page_url in full_data:
        entry = full_data[page_url]['content']
        # setup some counters and flags for processing the content
        content = []
        css_skip = False
        rating_skip = 0
        rating_seen = False
        css_skip_count = 0
        # loop over lines of text content in the entry (all page content now)
        for string in entry.stripped_strings:
            # skip lines saying "[[module css]]" as these are likely code
            # will skip until an "[[/module]]" is seen
            # but will only delete the content if the end tag is actually seen
            # so a lone start tag doesn't delete the whole page
            if "[[module css]]" in string.lower():
                css_skip = True
                continue
            # stop skipping if "[[/module]]" is seen
            if "[[/module]]" in string.lower():
                css_skip = False
                # actually delete the lines
                del content[-1 * css_skip_count:]
                continue
            # count the number of lines to delete once [[/module]] seen
            if css_skip:
                css_skip_count += 1

            # skip the rating module (4 "lines")
            if string == "rating:" and not rating_seen:
                rating_skip = 4
                rating_seen = True
                continue
            if rating_skip > 0:
                rating_skip -= 1
                continue

            # otherwise, this is just a line of content, add it to the list
            content.append(string)
        # join all the lines into one big string
        full_data[page_url]['content'] = " ".join(content)

        # add everything in full_data[page_url] to data[page_url]
        for key in full_data[page_url]:
            data[page_url][key] = full_data[page_url][key]

        if interface.verbosity >= 2:
            sys.stderr.write(f"{i}/{total}\n")
        if interface.verbosity == 1:
            sys.stderr.write(".")
            sys.stderr.flush()

        # get the rating (WhoRatedPageModule)
        data[page_url]['who_rated'] = get_rating_data(data[page_url]['pageId'], interface)
        time.sleep(1.0 / interface.request_rate)  # rate limit

        # get the wikisource (ViewSourceModule)
        data[page_url]['page_source'] = get_page_source(data[page_url]['pageId'], interface)
        time.sleep(1.0 / interface.request_rate)  # rate limit

        i += 1

    if interface.verbosity == 1:
        sys.stderr.write("\n")

    # skip pages without complete data (e.g. redirects and deleted pages)
    to_delete = []
    for page_url in data:
        if page_url not in full_data:
            # skip pages without full-fetch data
            to_delete.append(page_url)
        elif 'pageId' not in data[page_url]:
            # extra safeguard to prevent ID-less pages from making it to the database update step
            sys.stderr.write(f"WARNING: Skipping fetched page {page_url} with no pageId")
            to_delete.append(page_url)

    # delete separately to avoid "dictionary changed size during iteration" error
    for page_url in to_delete:
        del data[page_url]

    return data


def get_quick_data(interface, **kwargs):
    """Gets limited metadata for wiki pages, used for index update"""
    # Wikidot syntax for output format, see https://www.wikidot.com/doc-modules:listpages-module
    # |$$| is an arbitrary delimiter I use to make splitting easier
    module_body = "%%rating%%|$$|%%rating_votes%%|$$|%%created_by%%|$$|%%created_by_id%%|$$|%%revisions%%|$$|%%tags%% \
                  %%created_at%% %%updated_at%% %%link%%"

    if interface.verbosity > 0:
        sys.stderr.write(f"Fetching quick metadata with ListPages")
        sys.stderr.flush()
    if interface.verbosity >= 2:
        sys.stderr.write("\n")

    # get list of parsed HTML responses for each page of the listpages output
    soups = interface.paginate_module("list/ListPagesModule", perPage=200, module_body=module_body, **kwargs)

    if interface.verbosity > 0:
        sys.stderr.write(f"Processing quick metadata with ListPages\n")

    data = {}
    # loop over the paginated result
    for soup in soups:
        # get and iterate over all the .list-pages-item elements, as each contains a page
        entries = soup.find_all(class_="list-pages-item")
        for entry in entries:
            # get the first <p> element (containing the metadata info)
            metadata_raw = entry.p.contents
            # split 1st element
            metadata_arr = metadata_raw[0].split("|$$|")
            rating = int(metadata_arr[0])
            total_votes = int(metadata_arr[1])
            author_name = metadata_arr[2]
            revisions = int(metadata_arr[4])
            tags = metadata_arr[5].split(" ")
            created_at_timestamp = None
            for created_at_class in metadata_raw[1]['class']:
                if created_at_class.startswith("time_"):
                    created_at_timestamp = int(created_at_class.replace("time_", ""))
            updated_at_timestamp = None
            for updated_at_class in metadata_raw[3]['class']:
                if updated_at_class.startswith("time_"):
                    updated_at_timestamp = int(updated_at_class.replace("time_", ""))
            # convert author id to int, or None if missing
            try:
                author_id = int(metadata_arr[3])
            except ValueError:
                author_id = None
            # url is the only <a> element in there
            metadata = {"rating": rating, "votes": total_votes,
                        "url": entry.a.string, "author_name": author_name,
                        "author_id": author_id, "created_at": datetime.datetime.fromtimestamp(created_at_timestamp),
                        "updated_at": datetime.datetime.fromtimestamp(updated_at_timestamp), "revisions": revisions,
                        "tags": tags}
            # the page is non-canon if it has the "non-canon" tag
            metadata["canon"] = "non-canon" not in metadata["tags"]
            data[metadata["url"]] = metadata
    return data


def get_rating_data(page_id, interface, **kwargs):
    """Gets full rating data for the given page ID"""
    soups = interface.request_module("pagerate/WhoRatedPageModule", pageId=page_id, **kwargs)
    ratings = {}
    curuser_id = None
    for rating in soups.find_all("span"):
        if 'class' in rating.attrs and "printuser" in rating['class']:
            if "avatarhover" in rating['class']:
                curuser_id = rating.img['src'].split("userid=")[1].split("&")[0]
                ratings[curuser_id] = {"rating": 0, "name": rating.img['alt']}
            else:
                curuser_id = rating['data-id']
                ratings[curuser_id] = {"rating": 0, "name": "(account deleted)"}
        else:
            ratings[curuser_id]['rating'] = rating.text.strip()
            curuser_id = None
    return ratings


def get_page_source(page_id, interface, **kwargs):
    """Gets Wikidot page source (not rendered HTML) for the given page ID"""
    source = interface.request_module("viewsource/ViewSourceModule", page_id=page_id, raw="true", _doSoup=False,
                                      **kwargs)
    return str(source)


def get_pages_info(interface, page_urls):
    """Helper to limit request rate"""
    page_datas = {}
    i = 1
    total = len(page_urls)
    for page_url in page_urls:
        if page_url in page_datas:
            if interface.verbosity == 1:
                sys.stderr.write("\n")
            sys.stderr.write(f"NOTICE: Page {page_url} already indexed via redirect, skipping\n")
            continue
        page_datas[page_url] = wikidot_interface.get_page_info(page_url)
        if interface.verbosity >= 2:
            sys.stderr.write(f"{i}/{total}\n")
        if interface.verbosity == 1:
            sys.stderr.write(".")
            sys.stderr.flush()
        i += 1
        if 'pageId' not in page_datas[page_url]:
            if interface.verbosity == 1:
                sys.stderr.write("\n")
            sys.stderr.write(f"WARNING: Could not fetch page ID for {page_url}, skipping\n")
            del page_datas[page_url]
        else:
            # assemble the true URL from the params in the header script
            actual_url = page_url.split("://")[0] \
                + "://" + page_datas[page_url]['domain'] \
                + '/' + page_datas[page_url]['pageUnixName']
            # if this appears to be a redirect (true URL doesn't match requested URL)
            if actual_url != page_url and actual_url not in page_datas:
                # reassign it to the destination URL
                page_datas[actual_url] = page_datas[page_url]
                # delete the redirect URL from the dict
                del page_datas[page_url]
        time.sleep(1.0 / interface.request_rate)
    if interface.verbosity == 1:
        sys.stderr.write("\n")
    return page_datas


def check_page_existences(interface, page_ids):
    """Check if a page exists under the given page IDs"""
    existence = {}
    i = 0
    for page_id in page_ids:
        print(page_id)
        # try to request the page source
        resp = interface.module_request("viewsource/ViewSourceModule", page_id=page_id)
        # if the page doesn't exist, the status will be "no_page"
        if resp['status'] == 'no_page':
            existence[page_id] = False
        # if the page exists, the status will be "ok"
        elif resp['status'] == 'ok':
            existence[page_id] = True
        # set existence to None if something weird happened
        else:
            existence[page_id] = None
        i += 1
        if i < len(page_ids):  # sleep for timeout if there is another page to check
            time.sleep(1.0 / interface.request_rate)
    return existence
