import json
import time
import sys
import uuid

import requests
from bs4 import BeautifulSoup


def process_header_script(script):
    """Processes the JS in the header of a Wikidot page for useful parameters"""
    data = {}
    for line in script.split("\n"):
        if "WIKIREQUEST.info." in line:
            # these lines are of the form 'WIKIREQUEST.info.foo = "bar";'
            # 'WIKIREQUEST.info.foo = "bar";' would be added to the data dict as data['foo'] = "bar"
            line = line.split("=")
            key = line[0].strip().replace("WIKIREQUEST.info.", "")
            value = line[1].strip().strip('\'";')
            data[key] = str(value)
    return data


def get_page_info(page_url):
    """Gets info out of the block of JS at the top of the page"""
    page_data = {}
    # get and parse the page source
    resp = requests.get(page_url).text
    soup = BeautifulSoup(resp, "html.parser")
    scripts = soup.find_all("script")  # get the <script> tags
    for script in scripts:
        # the header script contains "pageId", so if this does it's probably the right one
        if script.string is not None and "pageId" in script.string:
            # process it
            page_data = process_header_script(script.string)
    # add the page content to the data as well
    page_data['content'] = soup.find_all("div", id="page-content")[0]
    return page_data


class WikidotInterface(object):
    """Handles the actual interfacing with Wikidot"""

    def __init__(self, site_url, verbosity=False, request_rate=0.5):
        """Sets up the object with a site and config options"""
        self.site_url = site_url  # URL of the wiki to send requests to
        self.verbosity = verbosity  # logging verbosity level
        self.request_rate = request_rate  # max requests/second to send

    def request_module(self, module, _bs4_process=True, **kwargs):
        """Parses and returns a single-page module response"""
        resp = self.module_request(module, **kwargs)['body']
        if _bs4_process:
            soup = BeautifulSoup(resp, "html.parser")
            return soup
        else:
            return resp

    def module_request(self, module, **kwargs):
        """Performs an ajax-module-connector.php request to a Wikidot module"""
        token = uuid.uuid4().hex  # generate random wikidot_token7

        # set wikidot_token7 as cookie
        cookies = requests.cookies.RequestsCookieJar()
        cookies.set('wikidot_token7', token, domain=self.site_url, path='/')

        # send request to http://site_url/ajax-module-connector.php with parameters
        # not sure what callbackIndex does, but I saw it on real Wikidot requests, so here it is
        # not sure if the Content-Type header is needed, but my browser was sending it and it seemed to help
        # moduleName is the name of the Wikidot module to send the request to (e.g. list/ListPagesModule)
        # wikidot_token7 is the same token from the cookie, these have to match or the request will be refused
        resp = requests.post('http://' + self.site_url + '/ajax-module-connector.php',
                             data={
                                 'wikidot_token7': token,
                                 'moduleName': module,
                                 'callbackIndex': '0',
                                 **kwargs},
                             cookies=cookies,
                             headers={'Content-Type': "application/x-www-form-urlencoded; charset=UTF-8"})

        # try to parse the response as json, log and error if it can't
        try:
            response = json.loads(resp.text)
        except json.JSONDecodeError:
            sys.stderr.write(resp.text + "\n")
            sys.stderr.flush()
            raise

        # "body" in the JSON contains the actual response content
        # return it, or fail if there is no such key
        try:
            return response
        except KeyError:
            sys.stderr.write(str(response) + "\n")
            sys.stderr.flush()
            raise

    def log_page_count(self, cur, last):
        """Logging helper for verbose progress meters"""
        # logging
        # no output on verbosity 0
        # write "." on verbosity 1
        # "X/Y" on verbosity 2
        if self.verbosity > 0:
            if self.verbosity == 1:
                sys.stderr.write(".")
                sys.stderr.flush()
            elif self.verbosity == 2:
                sys.stderr.write(str(cur) + "/" + str(last) + "\n")
            sys.stderr.flush()

    def paginate_module(self, module, **kwargs):
        """Iterates over all pages of a module response and returns them parsed in a list"""
        outputs = []  # array for all the pages
        cur = 1  # current page number
        while True:
            # request a page and parse it
            resp = self.module_request(module, p=cur, **kwargs)['body']
            soup = BeautifulSoup(resp, "html.parser")

            # add the page to the output list
            outputs.append(soup)

            # get the page navigation bar from the bottom of the page
            # since some pages have page controls on them as well,
            # get all page controls and use the last one
            page_controls = soup.find_all("span", class_="pager-no")

            # if there is a page control at all
            # TODO handle the case where the response is 1 page but there's a stray page control from the page content
            if len(page_controls) > 0:
                # get the actual page control and parse the page number
                page_control = page_controls[-1]
                page_nums = str(page_control.string).split(" ")  # ["page", "#", "of", "#"]
                cur = int(page_nums[1])
                last = int(page_nums[3])

                self.log_page_count(cur, last)

                # last page, done
                if cur == last:
                    break
            else:
                # one page, this is everything
                self.log_page_count(1, 1)
                break
            cur += 1
            # sleep to limit request rate
            time.sleep(1.0 / self.request_rate)
        if self.verbosity == 1:
            # the verbosity=1 output has no terminal newline, write one now
            sys.stderr.write("\n")
            sys.stderr.flush()
        return outputs
