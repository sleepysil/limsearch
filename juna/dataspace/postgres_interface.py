import datetime

import psycopg2


class PostgresInterface(object):
    """Handles the interfacing with the PostgreSQL DB"""

    def __init__(self, db, user, password, host, port):
        """Sets up the database connection parameters"""
        self.db = db
        self.user = user
        self.password = password
        self.host = host
        self.port = port
        conn_string = f"dbname={self.db} user={self.user} host={self.host} port={self.port}"
        if self.password is not None:
            conn_string += f" password={self.password}"
        self.conn = psycopg2.connect(conn_string)

    def commit(self):
        """Commits a transaction"""
        self.conn.commit()

    def exec_sql(self, sql, *args, fetch=True):
        """Wrapper around cursor.execute(),
        runs the given SQL with the given params and returns the result if fetch=True."""
        with self.conn.cursor() as cur:
            # assuming if a single dict was passed that this is using named parameters
            if len(args) == 1 and isinstance(args[0], dict):
                args = args[0]
            cur.execute(sql, args)
            if fetch:
                return cur.fetchall()

    def get_page_rating(self, page_id):
        """Gets the page rating from database given a page ID"""
        rating = self.exec_sql("SELECT sum(value) from ratings WHERE page_id=%s;", page_id)[0][0]
        if rating is None:
            rating = 0
        return rating

    def get_page_rec(self, page_id):
        page_data_arr = self.exec_sql("SELECT created_at, updated_at, \
                                        title, url, canon, users.name, \
                                        (regexp_match(pages.search_content, '(^(\\s*(\\S+\\s*){20}))'))[1] \
                                        AS highlights FROM pages LEFT JOIN users ON (pages.author_id=users.id) \
                                        WHERE pages.id=%s;", page_id)

        if len(page_data_arr) == 0:
            return None  # there is no such page

        page_data = page_data_arr[0]

        upvoters = self.exec_sql("select value, seen_at, users.name \
                                  FROM ratings LEFT JOIN users ON (ratings.user_id=users.id) \
                                  WHERE page_id=%s and value>0;", page_id)

        downvoters = self.exec_sql("select value, seen_at, users.name \
                                    FROM ratings LEFT JOIN users ON (ratings.user_id=users.id) \
                                    WHERE page_id=%s and value<0;", page_id)

        tags = self.exec_sql("select tags.name from pages inner join pages_tags \
                              on (pages.id=pages_tags.page_id) inner join tags \
                              on (pages_tags.tag_id=tags.id) where page_id=%s;", page_id)

        return {
            "created_at": page_data[0],
            "updated_at": page_data[1],
            "title": page_data[2],
            "url": page_data[3],
            "canon": page_data[4],
            "poster": page_data[5],
            "excerpt": page_data[6],
            "upvoters": upvoters,
            "downvoters": downvoters,
            "tags": tags
        }

    def get_page_id(self, url):
        """Gets the page ID from database given a URL"""
        page_id_rec = self.exec_sql("SELECT id FROM pages WHERE url=%s;", url)
        if len(page_id_rec) > 0:
            return page_id_rec[0][0]
        else:
            return None

    def get_page_id_for_title(self, title):
        """Gets the page ID from database given a URL"""
        page_id_rec = self.exec_sql("SELECT id FROM pages WHERE title=%s;", title)
        if len(page_id_rec) > 0:
            return page_id_rec[0][0]
        else:
            return None

    def delete_page(self, page_id):
        """Marks a page as deleted in the database, but does not actually remove it"""
        self.exec_sql("UPDATE pages SET deleted=TRUE WHERE id=%s;", page_id, fetch=False)

    def create_or_update_page(self, page_data):
        """Adds a page to the database, or updates it if it is already present."""
        self.create_or_update_user({'id': page_data['author_id'], 'name': page_data['author_name']})

        page_exists = len(self.exec_sql("SELECT 1 from pages WHERE id=%s;", page_data['pageId'])) > 0

        for tag in page_data['tags']:
            if tag == '':
                continue
            tag_exists = len(self.exec_sql("SELECT 1 from tags WHERE name=%s;", tag)) > 0
            if not tag_exists:
                self.exec_sql("INSERT INTO tags (name) VALUES (%s);", tag, fetch=False)

        if page_exists:
            self.exec_sql(
                "UPDATE pages SET (title, author_id, url, deleted, canon, search_content, \
                    page_source, rating, category_id, site_id, page_slug, theme_id, updated_at) = \
                (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) WHERE id=%s;",
                page_data['title'], page_data['author_id'], page_data['url'], False, page_data['canon'],
                page_data['content'],
                page_data['page_source'], page_data['rating'], page_data['categoryId'], page_data['siteId'],
                page_data['pageUnixName'],
                page_data['themeId'], page_data['updated_at'], page_data['pageId'], fetch=False)
        else:
            self.exec_sql("INSERT INTO pages (id, title, author_id, url, deleted, canon, search_content, page_source, \
                          rating, category_id, site_id, page_slug, theme_id, created_at, updated_at) \
            VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s);",
                          page_data['pageId'], page_data['title'], page_data['author_id'], page_data['url'], False,
                          page_data['canon'],
                          page_data['content'], page_data['page_source'], page_data['rating'], page_data['categoryId'],
                          page_data['siteId'],
                          page_data['pageUnixName'], page_data['themeId'], page_data['created_at'],
                          page_data['updated_at'], fetch=False)

        self.exec_sql("DELETE FROM pages_tags WHERE page_id=%s;", page_data['pageId'], fetch=False)
        for tag in page_data['tags']:
            if tag == '':
                continue
            tag_id = self.exec_sql("SELECT id FROM tags WHERE name=%s", tag)[0][0]
            self.exec_sql("INSERT INTO pages_tags (tag_id, page_id) VALUES (%s, %s);", tag_id, page_data['pageId'],
                          fetch=False)

    def update_page_metadata(self, page_data):
        """Updates metadata for a page. Cannot add a page as this does not include content."""
        self.create_or_update_user({'id': page_data['author_id'], 'name': page_data['author_name']})

        page_rec = self.exec_sql("SELECT id from pages WHERE url=%s;", page_data['url'])

        if len(page_rec) == 0:
            raise ValueError(f"Page {page_data['url']} does not exist")

        page_data['pageId'] = page_rec[0][0]  # quick data fetch does not get ID

        for tag in page_data['tags']:
            if tag == '':
                continue
            tag_exists = len(self.exec_sql("SELECT 1 from tags WHERE name=%s;", tag)) > 0
            if not tag_exists:
                self.exec_sql("INSERT INTO tags (name) VALUES (%s);", tag, fetch=False)

        self.exec_sql(
            "UPDATE pages SET (title, author_id, url, canon, rating, updated_at) = \
            (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s) WHERE id=%s;",
            page_data['title'], page_data['author_id'], page_data['url'], False, page_data['canon'],
            page_data['content'],
            page_data['page_source'], page_data['rating'], page_data['categoryId'], page_data['siteId'],
            page_data['pageUnixName'],
            page_data['themeId'], page_data['updated_at'], page_data['pageId'], fetch=False)

        self.exec_sql("DELETE FROM pages_tags WHERE page_id=%s;", page_data['pageId'], fetch=False)
        for tag in page_data['tags']:
            if tag == '':
                continue
            tag_id = self.exec_sql("SELECT id FROM tags WHERE name=%s", tag)[0][0]
            self.exec_sql("INSERT INTO pages_tags (tag_id, page_id) VALUES (%s, %s);", tag_id, page_data['pageId'],
                          fetch=False)

    def update_page_computed_metadata(self, page_data):
        pass

    def get_last_updated(self):
        result_tuples = self.exec_sql("SELECT last_updated from metadata;")

        timestamp = result_tuples[0][0].astimezone(datetime.timezone(datetime.timedelta()))
        now = datetime.datetime.now()
        diff = now - result_tuples[0][0]

        if diff.seconds // 3600 > 0:
            formatted_diff = f"{diff.seconds // 3600} hours"
        elif diff.seconds // 60 > 0:
            formatted_diff = f"{diff.seconds // 60} minutes"
        else:
            formatted_diff = f"{diff.seconds} seconds"

        return timestamp.strftime("%Y-%m-%d %H:%M:%S UTC") + " (" + formatted_diff + " ago)"

    def create_or_update_user(self, user_data):
        """Adds a user to the database, or updates it if it is already present."""
        if user_data['id'] is not None:  # skip null users
            user_exists = len(self.exec_sql("SELECT 1 from users WHERE id=%s;", user_data['id'])) > 0
            if user_exists:
                self.exec_sql("UPDATE users SET name = %s WHERE id=%s;",
                              user_data['name'], user_data['id'], fetch=False)
            else:
                self.exec_sql("INSERT INTO users (id, name) VALUES (%s, %s);", user_data['id'], user_data['name'],
                              fetch=False)

    def set_last_update_time(self, update_time):
        """Sets the 'last update time' in the metadata table, used for vote times."""
        self.exec_sql("UPDATE metadata SET last_updated=%s;", update_time, fetch=False)

    def create_or_update_rating(self, ratings, page_id, update_time):
        """Adds a page rating to the database, or updates it if it is already present."""
        last_update_rec = self.exec_sql("SELECT last_updated FROM metadata;")
        last_update = None
        if len(last_update_rec) > 0:
            last_update = last_update_rec[0][0]

        ratings_total = 0

        for rating_data in ratings:
            self.create_or_update_user({'id': rating_data['user_id'], 'name': rating_data['user_name']})

            # assuming the page is recorded already

            existing_rating = self.exec_sql("SELECT value from ratings WHERE user_id=%s AND page_id=%s;",
                                            rating_data['user_id'], page_id)
            rating_exists = len(existing_rating) > 0

            if rating_exists:
                # TODO fix this to use id from SELECT above
                if existing_rating[0][0] == rating_data['value']:
                    continue  # don't update the rating if it hasn't changed
                self.exec_sql(
                    "UPDATE ratings SET (value, seen_at, prev_update) = (%s, %s, %s) WHERE user_id=%s AND page_id=%s;",
                    rating_data['value'], update_time, last_update, rating_data['user_id'], page_id, fetch=False)
            else:
                self.exec_sql(
                    "INSERT INTO ratings (user_id, page_id, value, seen_at, prev_update) VALUES (%s, %s, %s, %s, %s);",
                    rating_data['user_id'], page_id, rating_data['value'], update_time, last_update, fetch=False)
            ratings_total += rating_data['value']
        if len(self.exec_sql("SELECT 1 FROM pages WHERE id=%s;", page_id)) > 0:
            self.exec_sql("UPDATE pages SET rating=%s WHERE id=%s;",
                          self.get_page_rating(page_id), page_id, fetch=False)
