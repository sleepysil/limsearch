import re
from typing import Union

from juna.dataspace import postgres_interface


def discord_format_query(handler, text_query=None, title_query=None, author_query=None, tags_query=None,
                         max_results=10):
    """Runs the query and formats output for Discord"""
    output = ""
    results = handler.search(text_query=text_query, title_query=title_query, author_query=author_query,
                             tags_query=tags_query)  # do the search

    # if there's more than one result, put the result count at the top
    if max_results > 1:
        output += str(min(len(results), max_results)) + " results\n\n"

    # process and format results
    count = 0
    for result in results:
        # if we've hit the result count limit, stop
        if count >= max_results:
            break

        # Linked title - Author (rating [+up, -down]) [Non-Canon]
        author_name = result["author"]
        if author_name is None:
            author_name = "(user deleted)"
        output += "### [" + result["title"] + "](" + result["url"] + ")" + " - " + author_name + " (" + (
            "+" if result['rating'] > 0 else "-") + str(
            result['rating']) + " [+" + str(result["upvotes"]) + ", -" + str(abs(result["downvotes"])) + "])"
        if not result["canon"]:
            output += " [Non-Canon]"
        output += "\n"

        # format the highlights (replace <b> tags with markdown)
        regex = re.compile("<b>")
        output += re.sub(regex, "**__", result['highlights']).replace("</b>", "__**") + "\n\n"
        count += 1
    return output


def compute_rating(rating, total_votes):
    """Computes up/downvote counts from rating and total votes"""
    # start by assuming all upvotes
    upvotes = rating
    downvotes = 0

    # if not enough votes
    # add 1 upvote + 1 downvote until count is right
    while total_votes > upvotes + downvotes:
        downvotes += 1
        upvotes += 1
    return upvotes, downvotes


class ParenthesesError(Exception):
    pass


class QuotesError(Exception):
    pass


class QuerySyntaxError(Exception):
    pass


def tokenize_query(query_raw):
    query_tokens = []

    cur_token = []

    operators = ["AND", "OR", "NOT"]

    if query_raw is None:
        return []  # no tokens in an empty query

    words = query_raw.split(" ")
    in_quotes = False
    paren_quotes = False
    in_parentheses = 0
    query_segment = ""
    for word in words:
        was_quoted = in_quotes

        if in_parentheses > 0 and not was_quoted:
            query_segment += " " + word
            if word.startswith('"') and not paren_quotes:
                paren_quotes = True
            if word.endswith('"') and paren_quotes:
                paren_quotes = False
            if paren_quotes:
                continue

            for char in word:
                if char == '(':
                    in_parentheses += 1
                    #print(f"paren level now at {in_parentheses}")
                if char == ')':
                    in_parentheses -= 1
                    #print(f"paren level now at {in_parentheses}")

            if in_parentheses == 0:  # closed all parentheses
                #print("parens closed")
                new_token = {"type": "token_group", "value": []}
                if query_segment != "":
                    new_token["value"] = tokenize_query(query_segment[:-1])  # remove last close paren
                query_tokens.append(new_token)
            continue

        if word.startswith('"'):  # or word.startswith("'"):
            in_quotes = True
            was_quoted = True
            if len(word) == 1:
                continue
            else:
                word = word[1:]

        if word.endswith('"'):  # or word.endswith("'"):
            in_quotes = False
            was_quoted = True
            if len(word) == 1:
                continue
            else:
                word = word[:-1]

        if word.startswith('(') and not in_quotes:
            if word != "()":
                if not word.endswith(')'):
                    in_parentheses += 1
                    #print(f"paren level now at {in_parentheses}")
                    if len(word) > 1:
                        word = word[1:]
                    query_segment = word
                else:  # (word)
                    word = word[1:-1]
            continue

        cur_token.append(word)

        if not in_quotes:
            if was_quoted or len(cur_token) != 1 or cur_token[0] not in operators:
                query_tokens.append({"type": "search_term", "value": cur_token})
            else:
                query_tokens.append({"type": "operator", "value": cur_token[0]})
            cur_token = []

    if in_parentheses > 0:
        raise ParenthesesError()

    if in_quotes:
        raise QuotesError()

    if cur_token:
        # type hint so my IDE doesn't complain about this
        joined_token: str = " ".join(cur_token)
        query_tokens.append(joined_token)

    return query_tokens


def convert_query(query_tokens) -> Union[str, None]:
    operators_to_psql = {"AND": "&", "OR": "|", "NOT": "!"}

    if query_tokens is None or query_tokens == []:
        return None

    prev_token = None

    query = []
    for token in query_tokens:

        if token["type"] == "search_term":
            query_str = " <-> ".join(token["value"])
            if len(token["value"]) > 1:
                query_str = "(" + query_str + ")"
            query.append(query_str)
        elif token["type"] == "operator":
            query.append(operators_to_psql[token["value"]])
        elif token["type"] == "token_group":
            query.append("(" + convert_query(token["value"]) + ")")
        else:
            pass

        if prev_token is not None and prev_token["type"] != "operator" and token["type"] != "operator":
            query.insert(len(query) - 1, "&")

        prev_token = token

    return " ".join(query)


def preproc_query(query):
    try:
        return convert_query(tokenize_query(query))
    except ParenthesesError:
        raise QuerySyntaxError(f"Unbalanced parentheses in {query}")
    except QuotesError:
        raise QuerySyntaxError(f"Unbalanced quotes in {query}")

class SearchHandler(object):
    """Manages various things for search stuff"""

    def __init__(self, db="dataspace", user="juna", password=None, host='localhost', port=5432):
        """Sets up the database connection for search"""
        self.psql = postgres_interface.PostgresInterface(db, user, password, host, port)

    def search(self, text_query=None, title_query=None, author_query=None, tags_query=None):
        """Performs a search using the given query parameters and returns the results"""
        sql_query = "SELECT pages.id FROM pages INNER JOIN pages_tags ON pages_tags.page_id=pages.id  \
                    INNER JOIN tags ON pages_tags.tag_id=tags.id INNER JOIN users ON users.id=pages.author_id WHERE"
        if text_query is not None:
            sql_query += " pages.search_data @@to_tsquery(%(text_query)s) AND"
        else:
            sql_query += " TRUE AND"

        if title_query is not None:
            sql_query += " pages.title_searchable @@ to_tsquery(%(title_query)s) AND"
        else:
            sql_query += " TRUE AND"

        if author_query is not None:
            sql_query += " users.name ILIKE %(author_query)s AND"
        else:
            sql_query += " TRUE AND"

        if tags_query is not None:
            sql_query += " tags.name IN %(tags_query)s;"
        else:
            sql_query += " TRUE;"

        if tags_query is None:
            tags_query = []  # not None to prevent error

        result_tuples = self.psql.exec_sql(sql_query, {"text_query": preproc_query(text_query),
                                                       "title_query": preproc_query(title_query),
                                                       "author_query": author_query,
                                                       "tags_query": tuple(tags_query)})

        ids = {rec[0] for rec in result_tuples}  # set comprehension to prevent duplicates

        results = []

        for page_id in ids:
            page_data = self.psql.exec_sql("SELECT title, url, canon, users.name, coalesce((select sum(value) FROM \
                                      ratings WHERE page_id=pages.id and value>0), 0) AS upvotes, coalesce((select \
                                      sum(value) FROM ratings WHERE page_id=pages.id AND value<0), 0) AS downvotes, \
                                      coalesce(ts_headline('english', pages.search_content, plainto_tsquery(%s)), \
                                      (regexp_match(pages.search_content, '(^(\\s*(\\S+\\s*){20}))'))[1]) \
                                      AS highlights FROM pages LEFT JOIN users ON (pages.author_id=users.id) \
                                      WHERE pages.id=%s;", text_query, page_id)[0]
            results.append({'title': page_data[0], 'url': page_data[1], 'author': page_data[3],
                            'rating': page_data[4] + page_data[5], 'upvotes': page_data[4], 'downvotes': page_data[5],
                            'canon': page_data[2], 'highlights': page_data[6]})
        self.psql.conn.commit()
        return results
